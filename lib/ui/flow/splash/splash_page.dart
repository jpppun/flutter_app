import 'package:flutter/material.dart';
import 'package:flutter_app/ui/flow/main/main_page.dart';
import 'package:flutter_app/core/flow/splash/splash_page_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_app/repository/flow/splash/splash_repository.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashPageState();
  }
}

class SplashPageState extends State<SplashPage> {
  SplashPageBloc _splashPageBloc;

  @override
  void initState() {
    super.initState();
    _splashPageBloc = BlocProvider.of(context);
    _splashPageBloc.add(SplashPageEvent.init);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: _splashPageBloc,
      builder: (context, state) {
        return Scaffold(body: Center(child: Text("Splash")));
      },
      listener: (context, state) => {
        if (state == SplashPageBlocState.finished)
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => MainPage(),
              ),
              (Route<dynamic> route) => false)
      },
    );
  }
}
