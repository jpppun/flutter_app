import 'package:flutter/material.dart';
import 'package:flutter_app/core/flow/signin/signin_page_bloc.dart';
import 'package:flutter_app/repository/user_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SigninPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SigninPageState();
  }
}

class SigninPageState extends State {
  SigninPageBloc signinPageBloc =
      SigninPageBloc(authicationRepository: AuthicationRepository());
  TextEditingController userNameInputTextController = TextEditingController();
  TextEditingController userPasswordInputTextController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: signinPageBloc,
      listenWhen: (previous, current) {
        return current is SignedIn ||
            previous is NotSignin &&
                current is NotSignin &&
                previous.isLogingin != current.isLogingin;
      },
      listener: (context, state) => {
        if (state is SignedIn)
          {Navigator.popUntil(context, (route) => route.isFirst)}
        else if (state is NotSignin)
          {
            if (state.isLogingin)
              {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(content: CircularProgressIndicator());
                    })
              }
            else
              {Navigator.pop(context)}
          }
      },
      builder: (context, state) {
        if (state is NotSignin) {
          return new Scaffold(
              body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("user name"),
              TextField(
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'user name'),
                controller: userNameInputTextController,
              ),
              Text("password"),
              TextField(
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'password'),
                controller: userPasswordInputTextController,
              ),
              RaisedButton(
                onPressed: () {
                  signinPageBloc.add(SinginClickedEvent(
                      userName: userNameInputTextController.text,
                      password: userPasswordInputTextController.text));
                },
                child: Text("signin"),
              )
            ],
          ));
        }
        if (state is SignedIn) {
          return Container();
        }
        return null;
      },
    );
  }
}
