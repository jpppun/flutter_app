
import 'package:flutter/material.dart';
import 'package:flutter_app/core/flow/main/main_page_bloc.dart';
import 'package:flutter_app/ui/flow/profile/profile_page.dart';
import 'package:flutter_app/ui/flow/home/home_page.dart';
import 'package:flutter_app/core/flow/profile/profile_page_bloc.dart';
import 'package:flutter_app/repository/user_repository.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainPageBloc mainPageBloc=MainPageBloc();
    return BlocBuilder<MainPageBloc, MainPageState>(
      bloc: mainPageBloc,
      builder: (context, state) {
        return buildWithState(mainPageBloc, state);
      },
    );
  }

  Widget buildWithState(MainPageBloc mainPageBloc, MainPageState state) {
    int currentIndex;
    switch (state) {
      case MainPageState.home:
        currentIndex = 0;
        break;
      case MainPageState.profile:
        currentIndex = 1;
        break;
    }
    Widget body;
    switch (state) {
      case MainPageState.home:
        body = HomePage();
        break;
      case MainPageState.profile:
        body = ProfilePage();
        break;
    }

    return Scaffold(
        body: BlocProvider<ProfilePageBloc>(create: (context)=>ProfilePageBloc(authicationRepository: AuthicationRepository()),child: body,),
        bottomNavigationBar: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), title: Text("home")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.portrait), title: Text("profile"))
            ],
            currentIndex: currentIndex,
            onTap: (value) => {
                  if (value == 0)
                    {mainPageBloc.add(MainPageEvent.home)}
                  else if (value == 1)
                    {mainPageBloc.add(MainPageEvent.profile)}
                }));
  }
}
