import 'package:flutter/material.dart';
import 'package:flutter_app/core/flow/signin/signin_page_bloc.dart';
import 'package:flutter_app/repository/user_repository.dart';
import 'package:flutter_app/ui/flow/signin/sigin_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_app/core/flow/profile/profile_page_bloc.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfilePageState();
  }
}

class ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    ProfilePageBloc profilePageBloc = BlocProvider.of(context);
    return BlocBuilder<ProfilePageBloc, ProfilePageBlocState>(
      builder: (context, state) {
        if (state is NotLoggedInState)
          return Scaffold(
            body: Center(
              child: Column(
                children: [
                  Icon(Icons.account_circle),
                  Text("you have not Logged in"),
                  RaisedButton(
                    onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return SigninPage();
                      }))
                    },
                    child: Text("click to login"),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
              ),
            ),
          );
        if (state is LoggedInState) {
          return Scaffold(body:Column(
            children: [
              Text(state.user.name),
              RaisedButton(
                onPressed: () {
                  profilePageBloc.add(LoggedOutClickEvent());
                },
                child: Text("sign out"),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
          )) ;
        }
      },
    );
  }
}
