import 'package:flutter/material.dart';
import 'package:flutter_app/ui/flow/main/main_page.dart';
import 'package:flutter_app/core/flow/main/main_page_bloc.dart';
import 'package:flutter_app/ui/flow/splash/splash_page.dart';
import 'package:flutter_app/core/flow/splash/splash_page_bloc.dart';
import 'package:flutter_app/repository/flow/splash/splash_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: SplashPage());
        home: MultiBlocProvider(providers: [
          BlocProvider<SplashPageBloc>(create: (context)=>SplashPageBloc(splashRepository: SplashRepository()),),
          BlocProvider<MainPageBloc>(create: (context) => MainPageBloc()),
        ], child: SplashPage()));
  }
}
