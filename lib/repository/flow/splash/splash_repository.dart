class SplashRepository {
  Future<bool> isSplashDataRdy() async {
    await Future.delayed(Duration(seconds: 2));
    return true;
  }
}
