import 'dart:async';

class AuthicationState {
  LoginStated loginStated;
  User user;
  AuthicationState({this.loginStated, this.user});
}

enum LoginStated { logged_in, not_logged_in }

class User {
  String name;
  User({this.name});
}

class LoginResult {
  bool isSuccess;
  User user;

  LoginResult({this.isSuccess, this.user});
}

class AuthicationRepository {
  AuthicationRepository._privateConstructor() {
    // _authicationStreamController.add(AuthicationState(loginStated: LoginStated.not_logged_in,user: null));//initial state is not logged in
    // Stream.periodic(Duration(seconds: 2),(x)=>x.abs()).listen((event) {
    //   if(event %2 ==0){
    //     _authicationStreamController.add(AuthicationState(loginStated: LoginStated.not_logged_in,user: null));
    //   }else{
    //     _authicationStreamController.add(AuthicationState(loginStated: LoginStated.logged_in,user: User(name:event.toString())));
    //   }
    // });
  }
  static final AuthicationRepository _instance =
      AuthicationRepository._privateConstructor();

  factory AuthicationRepository() {
    return _instance;
  }
  StreamController<AuthicationState> _authicationStreamController =
      StreamController<AuthicationState>();
  Stream<AuthicationState> get authicationStream =>
      _authicationStreamController.stream;

  Future<LoginResult> signin(String userName, String password) async {
    await Future.delayed(Duration(seconds: 1));
    if (userName == "123" && password == "123"){
      User user= User(name: "user1");
      _authicationStreamController.add(AuthicationState(loginStated: LoginStated.logged_in,user:user ));
      return LoginResult(isSuccess: true, user: user);
    }
    else
      return LoginResult(isSuccess: false, user: null);
  }
  Future<bool> signOut() async {
    // await Future.delayed(Duration(seconds: 1));
    _authicationStreamController.add(AuthicationState(loginStated: LoginStated.not_logged_in,user:null));
    return true;
  }

}
