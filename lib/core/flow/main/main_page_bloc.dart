import 'package:flutter_app/core/base/base_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


enum  MainPageEvent{
  home,profile
}

enum MainPageState{
    home,profile
}



class MainPageBloc extends BaseBloc<MainPageEvent,MainPageState>{
  @override
  MainPageState get initialState => MainPageState.home;

  @override
  Stream<MainPageState> mapEventToState(MainPageEvent event)async* {
       switch (event) {
        case MainPageEvent.home :
          yield MainPageState.home;
          break;
        case MainPageEvent.profile:
          yield MainPageState.profile;
      }
  }

  @override
  void dispose() {

  }
}