import 'package:flutter/material.dart';
import 'package:flutter_app/core/base/base_bloc.dart';
import 'package:flutter_app/repository/flow/splash/splash_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


enum SplashPageBlocState{
  loading,finished
}
enum SplashPageEvent{
  init
}

class SplashPageBloc extends BaseBloc<SplashPageEvent,SplashPageBlocState>{
  final SplashRepository splashRepository;

  SplashPageBloc({@required this.splashRepository});

  @override
  get initialState{
    return SplashPageBlocState.loading;
  }



  @override
  Stream<SplashPageBlocState> mapEventToState(SplashPageEvent event) async* {
    if(event == SplashPageEvent.init){
      bool isDataReady=  await splashRepository.isSplashDataRdy();
      if (isDataReady)
        yield SplashPageBlocState.finished;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }


}