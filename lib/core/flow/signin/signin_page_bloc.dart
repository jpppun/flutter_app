import 'package:flutter_app/core/base/base_bloc.dart';
import 'package:flutter_app/repository/user_repository.dart';

class SiginPageBlocEvent {}

class SignedInEvent extends SiginPageBlocEvent {
  User user;
  SignedInEvent({this.user});
}

class SinginClickedEvent extends SiginPageBlocEvent {
  String userName;
  String password;
  SinginClickedEvent({this.password, this.userName});
}

class SiginPageBlocState {}

class NotSignin extends SiginPageBlocState {
  bool isLogingin;
  NotSignin({this.isLogingin});
}

class SignedIn extends SiginPageBlocState {
  User user;
  SignedIn({this.user});
}

class SigninPageBloc extends BaseBloc<SiginPageBlocEvent, SiginPageBlocState> {
  AuthicationRepository authicationRepository;

  SigninPageBloc({this.authicationRepository});

  @override
  get initialState => NotSignin();

  @override
  Stream<SiginPageBlocState> mapEventToState(SiginPageBlocEvent event) async* {
    if (event is SinginClickedEvent) {
      yield NotSignin(isLogingin: true);
      LoginResult result = await authicationRepository.signin(event.userName, event.password);
      if (result.isSuccess) {
        yield SignedIn(user: result.user);
      } else {
        yield NotSignin(isLogingin: false);
      }
    } else {
      throw UnimplementedError();
    }
  }
}
