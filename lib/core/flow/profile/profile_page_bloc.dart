import 'package:flutter_app/core/base/base_bloc.dart';
import 'package:flutter_app/repository/user_repository.dart';

class ProfilePageBlocEvent {}

class LoggedInEvent extends ProfilePageBlocEvent {
  User user;
  LoggedInEvent({this.user});
}

class LoggedOutEvent extends ProfilePageBlocEvent {}

class LoggedOutClickEvent extends ProfilePageBlocEvent {}

class ProfilePageBlocState {}

class NotLoggedInState extends ProfilePageBlocState {}

class LoggedInState extends ProfilePageBlocState {
  User user;
  LoggedInState({this.user});
}

class ProfilePageBloc
    extends BaseBloc<ProfilePageBlocEvent, ProfilePageBlocState> {
  AuthicationRepository authicationRepository;

  ProfilePageBloc({this.authicationRepository}) {
    authicationRepository.authicationStream.listen((event) {
      if (event.loginStated == LoginStated.logged_in) {
        add(LoggedInEvent(user: event.user));
      }
      if (event.loginStated == LoginStated.not_logged_in) {
        add(LoggedOutEvent());
      }
    });
  }

  @override
  ProfilePageBlocState get initialState => NotLoggedInState();

  @override
  Stream<ProfilePageBlocState> mapEventToState(
      ProfilePageBlocEvent event) async* {
    if (event is LoggedInEvent) {
      yield LoggedInState(user: event.user);
    }
    if (event is LoggedOutEvent) {
      yield NotLoggedInState();
    }
    if (event is LoggedOutClickEvent) {
      bool logoutSuccess = await authicationRepository.signOut();
      if (logoutSuccess) yield NotLoggedInState();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
